import keras


class LossLoggerCallback(keras.callbacks.Callback):
    def __init__(self):
        super(LossLoggerCallback, self).__init__()
        self.losses = dict()


    def on_train_begin(self, logs=None):
        for loss_name in list(logs.keys()):
            self.losses[loss_name] = []
    def on_train_batch_end(self, batch, logs=None):
        for loss_name in list(logs.keys()):
            pass