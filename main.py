from VAE import CW2, CWAE, VAE, LCW
from utils import log_results
import keras
import tensorflow as tf
import numpy as np
import os
from datetime import datetime

os.environ["KERAS_BACKEND"] = "tensorflow"

# -------BEGIN PARAMETERS-------

args = {"load_model": False,            # to load a model given by model_path
        "model_path": "results/cw2/2024_03_27__10_42_49/model.keras",       # only relevant when load_model True
        "dataset": "mnist",                     # mnist, fashion_mnist
        "sample_amount": 70000,                 # sample amount taken from the dataset(train and test combined)
        "latent_dim": 24,
        "noise_dim": 24,                        # input size for noise generator; only used in lcw
        "max_epochs": 500,
        "epochs": None,                         # actual epochs done in training(may be less than max because of patience)
        "batch_size": 128,
        "patience": 5,
        "learning_rate": 0.0005,
        "results_dir": f"results/",             # new dir for model_type and timestamp will be created if necessary
        "model_type": "cw2",                    # only lcw and cw2 atm
        "architecture_type": "cw2",             # cw2, standard, coneshaped, inverse_coneshaped
        "bias": True,                           # wether to use a bias neuron or not; lcw generator always has bias neurons
        "batch_norm": True,                     # wether to use batchnormalization layer after each layer or not
        "tsne_amount": 500,
        "perplexity": 10,
        "notes": "Fixed again silverman, separate uses of normal and not, stddev calculated on the correct. different lr"}

# -------END PARAMETERS-------

timestamp = datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
args["save_dir"] = args["results_dir"] + f'{args["model_type"]}/{timestamp}/'
tensorboard_log_dir = args["save_dir"] + 'tensorboard/'
history = None

if args["dataset"] == "mnist":
    (x_train, y_train), (x_test, _) = keras.datasets.mnist.load_data()
elif args["dataset"] == "fashion_mnist":
    (x_train, y_train), (x_test, _) = keras.datasets.fashion_mnist.load_data()
mnist_digits = np.concatenate([x_train, x_test], axis=0)[0:args["sample_amount"]]

# add stddev for the different silverman rule of thumb
# num_images = mnist_digits.shape[0]
# flattened_images = mnist_digits.reshape(num_images, -1)
# args["stddev"] = np.std(flattened_images)

mnist_digits = np.expand_dims(mnist_digits, -1).astype("float32") / 255


if args["model_type"] == "lcw":
    if args["load_model"]:
        model = tf.keras.saving.load_model(args["model_path"])
    else:
        # train cw2 as basis for lcw
        cw2_model = CW2(args)
        cw2_model.compile(optimizer=keras.optimizers.Adam(learning_rate=args["learning_rate"]))
        es_callback = keras.callbacks.EarlyStopping(monitor='total_loss', patience=args["patience"], mode="min")
        ts_callback = keras.callbacks.TensorBoard(log_dir=tensorboard_log_dir)
        cw2_model.fit(mnist_digits, epochs=args["max_epochs"], batch_size=args["batch_size"], callbacks=[es_callback])

        # train lcw generator with data anew
        model = LCW(cw2_model.encoder, cw2_model.decoder, args)
        model.compile(optimizer=keras.optimizers.Adam(learning_rate=args["learning_rate"]))
        es2_callback = keras.callbacks.EarlyStopping(monitor='cw_reconstruction_loss', patience=args["patience"], mode="min")
        ts2_callback = keras.callbacks.TensorBoard(log_dir=tensorboard_log_dir)
        history = model.fit(mnist_digits, epochs=args["max_epochs"], batch_size=args["batch_size"], callbacks=[es2_callback])

        args["epochs"] = len(history.history["total_loss"])

elif args["model_type"] == "cw2":
    model = CW2(args)

    if args["load_model"]:
        model = keras.models.load_model(args["model_path"])
    else:
        model.compile(optimizer=keras.optimizers.Adam(learning_rate=args["learning_rate"]))
        es_callback = keras.callbacks.EarlyStopping(monitor='total_loss', patience=args["patience"], mode="min")
        ts_callback = keras.callbacks.TensorBoard(log_dir=tensorboard_log_dir)
        history = model.fit(mnist_digits, epochs=args["max_epochs"], batch_size=args["batch_size"], callbacks=[es_callback, ts_callback])

        args["epochs"] = len(history.history["total_loss"])

# log results, including creating plots
log_results(model, history, args, x_train, y_train)
