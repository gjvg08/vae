import unittest
from cw_cost import silverman_rule_of_thumb, cw_normality, cw_sampling_lcw
import tensorflow as tf


class TestSilvermanRuleOfThumb(unittest.TestCase):
    def test_normal_distribution_scenario_with_non_tensor_count(self):
        count, stddev = (64, tf.constant(1.0))

        gamma_value = silverman_rule_of_thumb(stddev, count)

        self.assertAlmostEqual(gamma_value.numpy(), 0.21248091634, places=5)  # add assertion here

    def test_normal_distribution_scenario_with_tensor_count(self):
        count, stddev = (tf.constant(32), tf.constant(1.0))

        gamma_value = silverman_rule_of_thumb(stddev, count)

        self.assertAlmostEqual(gamma_value.numpy(), 0.28037025, places=5)

    def test_not_normal_scenario_without_tensor(self):
        count, stddev = (16, 0.1)

        gamma_value = silverman_rule_of_thumb(stddev, count)

        self.assertAlmostEqual(gamma_value.numpy(), 0.00369950762, places=8)


class TestNormalityMeasureNoScale(unittest.TestCase):

    def test_zero_vector_and_ones_vector(self):
        N, D = (2, 32)
        gamma = tf.constant(0.5, dtype=tf.float64)
        sample = tf.concat([tf.zeros((1, D)), tf.ones((1, D)), tf.zeros((N - 2, D))], axis=0)

        normality_distance = cw_normality(sample, gamma)

        self.assertAlmostEqual(normality_distance.numpy(), 0.20768175975, places=4)

    def test_distance_for_standard_normal_samples_are_smaller_than_these_for_uniform_ones_when_using_the_same_gamma(
            self):
        N, D = (100, 40)
        standard_sample = tf.random.normal((N, D))
        uniform_sample = tf.random.uniform((N, D))
        gamma = tf.constant(1.0, dtype=tf.float64)

        standard_value = cw_normality(standard_sample, gamma)
        uniform_value = cw_normality(uniform_sample, gamma)

        self.assertLess(standard_value, uniform_value)

    def test_result_is_zero_dim_tensor(self):
        N, D = (16, 128)
        gamma = tf.constant(0.2, dtype=tf.float64)
        sample = tf.random.uniform((N, D))

        normality_distance = cw_normality(sample, gamma)

        self.assertEqual(normality_distance.ndim, 0)

    def test_behavior_for_sample_containing_only_zeros(self):
        N, D = (8, 32)
        gamma = tf.constant(1.0, dtype=tf.float64)
        sample = tf.zeros((N, D))

        normality_distance = cw_normality(sample, gamma)

        self.assertAlmostEqual(normality_distance.numpy(), 0.07411361933, places=5)


class TestSampleDistance(unittest.TestCase):

    def test_sample_from_itself_is_zero(self):
        N, D = (2, 32)
        sample = tf.random.normal((N, D))
        gamma = 1.0

        distance = cw_sampling_lcw(sample, sample, gamma)

        self.assertAlmostEqual(distance.numpy(), 0.0, places=5)

    def test_symmetry(self):
        N, D = (128, 16)
        first_sample = tf.random.normal((N, D))
        second_sample = tf.random.normal((N, D))
        gamma = 0.3

        first_distance = cw_sampling_lcw(first_sample, second_sample, gamma)
        second_distance = cw_sampling_lcw(second_sample, first_sample, gamma)

        self.assertAlmostEqual(first_distance.numpy(), second_distance.numpy(), places=5)

    def test_triangle_condition(self):
        N, D = (64, 8)
        first_sample = tf.random.normal((N, D))
        second_sample = tf.random.normal((N, D))
        third_sample = tf.random.normal((N, D))
        gamma = 0.2

        first_distance = cw_sampling_lcw(first_sample, second_sample, gamma)
        second_distance = cw_sampling_lcw(second_sample, third_sample, gamma)
        third_distance = cw_sampling_lcw(first_sample, third_sample, gamma)

        self.assertLessEqual(third_distance.numpy(), first_distance.numpy() + second_distance.numpy())

    def test_simple_scenario(self):
        gamma = tf.constant(0.25, dtype=tf.float64)
        first_sample = tf.constant([[0.8552, -0.4313, 0.6838, -1.3297],
                                    [-0.9460, 0.0400, -0.3971, -0.6276],
                                    [0.5923, -0.5162, 0.3167, -1.9416],
                                    [0.0510, -2.1722, 0.0046, -0.8541],
                                    [-0.8097, -1.6507, -1.7508, -1.0506],
                                    [0.5821, -1.0767, 1.4038, 0.2506],
                                    [-0.0436, -0.5890, 0.0834, -0.5067],
                                    [-1.5162, -0.4734, -1.3553, -0.4061]], dtype=tf.float64)
        second_sample = tf.constant([[0.0627, 0.7577, -1.4780, 1.0068],
                                     [0.3011, 0.6528, -0.4111, -0.3062],
                                     [-0.7347, 1.0980, 0.4572, -2.5876],
                                     [-0.9036, 0.2979, -0.2190, 0.0227],
                                     [-0.8610, -1.4610, 1.9940, 1.1780],
                                     [-0.3901, -0.6019, -0.3916, -0.4636],
                                     [0.9056, -0.5749, 0.1444, -1.5109],
                                     [1.6627, 0.8215, -0.5497, -1.7971]], dtype=tf.float64)

        result = cw_sampling_lcw(first_sample, second_sample, gamma)

        self.assertAlmostEqual(result.numpy(), 0.0778394117951393, places=5)

    def test_simple_scenario_2(self):
        first_sample = tf.constant([[0.7877, 0.5314, -0.4811, 0.8969, 0.2553, 0.1176],
                                    [0.0945, 1.2884, -2.1206, 1.4172, -1.0934, -1.0650],
                                    [0.2653, 0.4681, -0.5184, -1.1459, -0.9426, -0.7228],
                                    [0.8244, -0.3308, 0.2718, -2.0928, 1.4914, 1.0128]], dtype=tf.float64)

        second_sample = tf.constant([[0.1651, -0.2732, 1.7607, -0.0764, -1.1410, 0.8466],
                                     [0.3135, -0.3145, -1.0066, -0.1305, 1.8563, 1.0151],
                                     [1.0817, 0.5058, 0.1595, 1.1838, 0.0043, 0.2077],
                                     [-0.1303, 1.4732, -3.2696, -1.2283, 0.0055, -2.4148]], dtype=tf.float64)

        gamma = 0.123

        result = cw_sampling_lcw(first_sample, second_sample, gamma)

        self.assertAlmostEqual(result.numpy(), 0.21837042272090912, places=5)

    def test_two_normal_distribution_are_closer_than_uniform(self):
        N, D = 128, 32
        first_normal_sample = tf.random.normal((N, D))
        second_normal_sample = tf.random.normal((N, D))
        uniform_sample = tf.random.uniform((N, D))
        gamma = 0.0259

        normal_distance = cw_sampling_lcw(first_normal_sample, second_normal_sample, gamma)
        uniform_to_normal = cw_sampling_lcw(first_normal_sample, uniform_sample, gamma)

        self.assertLessEqual(normal_distance, uniform_to_normal)


if __name__ == '__main__':
    unittest.main()
