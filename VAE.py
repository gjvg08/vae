import keras
import numpy as np
import tensorflow as tf
from cw_cost import cw_sampling_silverman, cw_normality, mean_squared_euclidean_norm_reconstruction_error, cw_sampling, \
    silverman_rule_of_thumb_normal, cw_sampling_lcw, silverman_rule_of_thumb
from architectures import get_architecture, LcwGenerator


# loading and saving is still bugged. get config is only relevant for that and has no other practical use here

@tf.keras.saving.register_keras_serializable()
class LCW(keras.Model):
    """Implementation of Latent-Cramer-Wold Autoencoder. Can be based on any autoencoder supplied through the
    constructor."""

    def __init__(self, encoder, decoder, args):
        """Encoder and decoder should be based on the same args as the generator. They should already be trained."""
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.generator = LcwGenerator(args).build()
        self.gamma = silverman_rule_of_thumb_normal(args["sample_amount"])
        self.args = args
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="cw_reconstruction_loss"
        )

    def call(self, inputs, **kwargs):
        z = self.encoder(input)
        # this creates gaussian noise in the right format for training
        batch_size = tf.shape(z)[0]
        noise_np = np.random.normal(0, 1, size=self.args["noise_dim"])
        noise_tf = tf.expand_dims(tf.convert_to_tensor(noise_np), axis=0)
        noise_tf = tf.repeat(noise_tf, repeats=batch_size, axis=0)
        noise_z = self.generator(noise_tf)
        return z, noise_z

    @property
    def metrics(self):
        return [
            self.reconstruction_loss_tracker
        ]

    def train_step(self, data):
        with tf.GradientTape() as tape:
            z, noise_z = self(data)
            cw_reconstruction_loss = tf.math.log(cw_sampling_lcw(z, noise_z, self.gamma))

        grads = tape.gradient(cw_reconstruction_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.reconstruction_loss_tracker.update_state(cw_reconstruction_loss)
        return {
            "cw_reconstruction_loss": self.reconstruction_loss_tracker.result()
        }


@tf.keras.saving.register_keras_serializable()
class CW2(keras.Model):
    """Implementation of the Cramer-Wold² Autoencoder."""

    def __init__(self, args):
        super().__init__()
        self.encoder, self.decoder = get_architecture(args)
        # self.gamma = silverman_rule_of_thumb(args["stddev"], args["sample_amount"])
        # self.gamma = silverman_rule_of_thumb_normal(args["sample_amount"])
        self.args = args
        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="cw_reconstruction_loss"
        )
        self.cw_loss_tracker = keras.metrics.Mean(name="cw_loss")

    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.cw_loss_tracker,
        ]

    def call(self, input):
        z = self.encoder(input)
        reconstruction = self.decoder(z)
        return z, reconstruction

    def train_step(self, data):
        with tf.GradientTape() as tape:
            z, reconstruction = self(data)

            cw_reconstruction_loss = tf.math.log(cw_sampling_silverman(reconstruction, data))
            lambda_val = 1
            cw_loss = lambda_val * tf.math.log(cw_normality(z))
            total_loss = cw_reconstruction_loss + cw_loss

        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(cw_reconstruction_loss)
        self.cw_loss_tracker.update_state(cw_loss)

        return {
            "total_loss": self.total_loss_tracker.result(),
            "cw_reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "cw_loss": self.cw_loss_tracker.result(),
        }


@tf.keras.saving.register_keras_serializable()
class CWAE(keras.Model):
    """Implementation of the Cramer-Wold Autoencoder. This uses MSE for reconstruction error."""

    def __init__(self, args):
        super().__init__()
        self.encoder, self.decoder = get_architecture(args)
        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="cw_reconstruction_loss"
        )
        self.cw_loss_tracker = keras.metrics.Mean(name="cw_loss")

    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.cw_loss_tracker,
        ]

    def train_step(self, data):
        with tf.GradientTape() as tape:
            z = self.encoder(data)
            reconstruction = self.decoder(z)
            cw_reconstruction_loss = tf.math.log(
                mean_squared_euclidean_norm_reconstruction_error(data, reconstruction))
            lambda_val = 1
            cw_loss = lambda_val * tf.math.log(cw_sampling(z))
            total_loss = tf.cast(cw_reconstruction_loss,
                                 dtype=tf.float32) + cw_loss

        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(cw_reconstruction_loss)
        self.cw_loss_tracker.update_state(cw_loss)
        return {
            "total_loss": self.total_loss_tracker.result(),
            "cw_reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "cw_loss": self.cw_loss_tracker.result(),
        }


@tf.keras.saving.register_keras_serializable()
class VAE(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.encoder, self.decoder = get_architecture(args)
        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="reconstruction_loss"
        )
        self.kl_loss_tracker = keras.metrics.Mean(name="kl_loss")

    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.kl_loss_tracker,
        ]

    def train_step(self, data):
        with tf.GradientTape() as tape:
            z_mean, z_log_var, z = self.encoder(data)
            reconstruction = self.decoder(z)
            reconstruction_loss = tf.reduce_mean(
                tf.reduce_sum(
                    keras.losses.binary_crossentropy(data, reconstruction),
                    axis=(1, 2),
                )
            )
            kl_loss = -0.5 * (1 + z_log_var -
                              tf.square(z_mean) - tf.exp(z_log_var))
            kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))
            total_loss = reconstruction_loss + kl_loss

        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(reconstruction_loss)
        self.kl_loss_tracker.update_state(kl_loss)
        return {
            "total_loss": self.total_loss_tracker.result(),
            "reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "kl_loss": self.kl_loss_tracker.result(),
        }
