Model: "encoder"
__________________________________________________________________________________________________
 Layer (type)                   Output Shape         Param #     Connected to                     
==================================================================================================
 input_3 (InputLayer)           [(None, 28, 28, 1)]  0           []                               
                                                                                                  
 flatten_1 (Flatten)            (None, 784)          0           ['input_3[0][0]']                
                                                                                                  
 dense_7 (Dense)                (None, 256)          200960      ['flatten_1[0][0]']              
                                                                                                  
 batch_normalization_6 (BatchNo  (None, 256)         1024        ['dense_7[0][0]']                
 rmalization)                                                                                     
                                                                                                  
 activation_6 (Activation)      (None, 256)          0           ['batch_normalization_6[0][0]',  
                                                                  'batch_normalization_7[0][0]',  
                                                                  'batch_normalization_8[0][0]']  
                                                                                                  
 dense_8 (Dense)                (None, 256)          65792       ['activation_6[0][0]']           
                                                                                                  
 batch_normalization_7 (BatchNo  (None, 256)         1024        ['dense_8[0][0]']                
 rmalization)                                                                                     
                                                                                                  
 dense_9 (Dense)                (None, 256)          65792       ['activation_6[1][0]']           
                                                                                                  
 batch_normalization_8 (BatchNo  (None, 256)         1024        ['dense_9[0][0]']                
 rmalization)                                                                                     
                                                                                                  
 z (Dense)                      (None, 24)           6168        ['activation_6[2][0]']           
                                                                                                  
==================================================================================================
Total params: 341,784
Trainable params: 340,248
Non-trainable params: 1,536
__________________________________________________________________________________________________


Model: "encoder"
__________________________________________________________________________________________________
 Layer (type)                   Output Shape         Param #     Connected to                     
==================================================================================================
 input_4 (InputLayer)           [(None, 24)]         0           []                               
                                                                                                  
 dense_10 (Dense)               (None, 256)          6400        ['input_4[0][0]']                
                                                                                                  
 batch_normalization_9 (BatchNo  (None, 256)         1024        ['dense_10[0][0]']               
 rmalization)                                                                                     
                                                                                                  
 activation_7 (Activation)      (None, 256)          0           ['batch_normalization_9[0][0]',  
                                                                  'batch_normalization_10[0][0]', 
                                                                  'batch_normalization_11[0][0]'] 
                                                                                                  
 dense_11 (Dense)               (None, 256)          65792       ['activation_7[0][0]']           
                                                                                                  
 batch_normalization_10 (BatchN  (None, 256)         1024        ['dense_11[0][0]']               
 ormalization)                                                                                    
                                                                                                  
 dense_12 (Dense)               (None, 256)          65792       ['activation_7[1][0]']           
                                                                                                  
 batch_normalization_11 (BatchN  (None, 256)         1024        ['dense_12[0][0]']               
 ormalization)                                                                                    
                                                                                                  
 dense_13 (Dense)               (None, 784)          201488      ['activation_7[2][0]']           
                                                                                                  
 reshape_1 (Reshape)            (None, 28, 28, 1)    0           ['dense_13[0][0]']               
                                                                                                  
==================================================================================================
Total params: 342,544
Trainable params: 341,008
Non-trainable params: 1,536
__________________________________________________________________________________________________
