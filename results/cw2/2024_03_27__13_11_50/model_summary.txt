Model: "encoder"
__________________________________________________________________________________________________
 Layer (type)                   Output Shape         Param #     Connected to                     
==================================================================================================
 input_1 (InputLayer)           [(None, 28, 28, 1)]  0           []                               
                                                                                                  
 flatten (Flatten)              (None, 784)          0           ['input_1[0][0]']                
                                                                                                  
 dense (Dense)                  (None, 256)          200960      ['flatten[0][0]']                
                                                                                                  
 batch_normalization (BatchNorm  (None, 256)         1024        ['dense[0][0]']                  
 alization)                                                                                       
                                                                                                  
 activation (Activation)        (None, 256)          0           ['batch_normalization[0][0]',    
                                                                  'batch_normalization_1[0][0]',  
                                                                  'batch_normalization_2[0][0]']  
                                                                                                  
 dense_1 (Dense)                (None, 256)          65792       ['activation[0][0]']             
                                                                                                  
 batch_normalization_1 (BatchNo  (None, 256)         1024        ['dense_1[0][0]']                
 rmalization)                                                                                     
                                                                                                  
 dense_2 (Dense)                (None, 256)          65792       ['activation[1][0]']             
                                                                                                  
 batch_normalization_2 (BatchNo  (None, 256)         1024        ['dense_2[0][0]']                
 rmalization)                                                                                     
                                                                                                  
 z (Dense)                      (None, 24)           6168        ['activation[2][0]']             
                                                                                                  
==================================================================================================
Total params: 341,784
Trainable params: 340,248
Non-trainable params: 1,536
__________________________________________________________________________________________________


Model: "decoder"
__________________________________________________________________________________________________
 Layer (type)                   Output Shape         Param #     Connected to                     
==================================================================================================
 input_2 (InputLayer)           [(None, 24)]         0           []                               
                                                                                                  
 dense_3 (Dense)                (None, 256)          6400        ['input_2[0][0]']                
                                                                                                  
 batch_normalization_3 (BatchNo  (None, 256)         1024        ['dense_3[0][0]']                
 rmalization)                                                                                     
                                                                                                  
 activation_1 (Activation)      (None, 256)          0           ['batch_normalization_3[0][0]',  
                                                                  'batch_normalization_4[0][0]',  
                                                                  'batch_normalization_5[0][0]']  
                                                                                                  
 dense_4 (Dense)                (None, 256)          65792       ['activation_1[0][0]']           
                                                                                                  
 batch_normalization_4 (BatchNo  (None, 256)         1024        ['dense_4[0][0]']                
 rmalization)                                                                                     
                                                                                                  
 dense_5 (Dense)                (None, 256)          65792       ['activation_1[1][0]']           
                                                                                                  
 batch_normalization_5 (BatchNo  (None, 256)         1024        ['dense_5[0][0]']                
 rmalization)                                                                                     
                                                                                                  
 dense_6 (Dense)                (None, 784)          201488      ['activation_1[2][0]']           
                                                                                                  
 reshape (Reshape)              (None, 28, 28, 1)    0           ['dense_6[0][0]']                
                                                                                                  
==================================================================================================
Total params: 342,544
Trainable params: 341,008
Non-trainable params: 1,536
__________________________________________________________________________________________________
