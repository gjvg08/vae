import tensorflow as tf
import keras
from keras import layers


class Sampling(layers.Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.random.normal(shape=(batch, dim))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon


@tf.keras.saving.register_keras_serializable()
class StandardEncoder(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.activation = layers.Activation("relu")
        self.flatten = layers.Flatten()
        self.conv1 = layers.Conv2D(32, 3, strides=2, padding="same", use_bias=args["bias"])
        self.batchnorm1 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.conv2 = layers.Conv2D(64, 3, strides=2, padding="same", use_bias=args["bias"])
        self.batchnorm2 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense1 = layers.Dense(32, use_bias=args["bias"])
        self.batchnorm3 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense2 = layers.Dense(args["latent_dim"], name="z")

    def build(self):
        encoder_inputs = keras.Input(shape=(28, 28, 1))
        x = self.conv1(encoder_inputs)
        x = self.batchnorm1(x)
        x = self.activation(x)
        x = self.conv2(x)
        x = self.batchnorm2(x)
        x = self.activation(x)
        x = self.flatten(x)
        x = self.dense1(x)
        x = self.batchnorm3(x)
        x = self.activation(x)
        z = self.dense2(x)
        encoder = keras.Model(encoder_inputs, [z], name="encoder")
        return encoder


@tf.keras.saving.register_keras_serializable()
class StandardDecoder(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.latent_dim = args["latent_dim"]
        self.activation = layers.Activation("relu")
        self.dense1 = layers.Dense(7 * 7 * 64, use_bias=args["bias"])
        self.batchnorm1 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.reshape = layers.Reshape((7, 7, 64))
        self.conv1 = layers.Conv2DTranspose(64, 3, strides=2, padding="same", use_bias=args["bias"])
        self.batchnorm2 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.conv2 = layers.Conv2DTranspose(32, 3, strides=2, padding="same", use_bias=args["bias"])
        self.batchnorm3 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.conv3 = layers.Conv2DTranspose(1, 3, activation="sigmoid", padding="same", use_bias=args["bias"])

    def build(self):
        latent_inputs = keras.Input(shape=(self.latent_dim,))
        x = self.dense1(latent_inputs)
        x = self.batchnorm1(x)
        x = self.activation(x)
        x = self.reshape(x)
        x = self.conv1(x)
        x = self.batchnorm2(x)
        x = self.activation(x)
        x = self.conv2(x)
        x = self.batchnorm3(x)
        x = self.activation(x)
        decoder_outputs = self.conv3(x)
        encoder = keras.Model(latent_inputs, decoder_outputs, name="decoder")
        return encoder


@tf.keras.saving.register_keras_serializable()
class Cw2Encoder(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.activation = layers.Activation("relu")
        self.flatten = layers.Flatten()
        self.dense1 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm1 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense2 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm2 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense3 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm3 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense4 = layers.Dense(args["latent_dim"], name="z")

    def build(self):
        encoder_inputs = keras.Input(shape=(28, 28, 1))
        x = self.flatten(encoder_inputs)
        x = self.dense1(x)
        x = self.batchnorm1(x)
        x = self.activation(x)
        x = self.dense2(x)
        x = self.batchnorm2(x)
        x = self.activation(x)
        x = self.dense3(x)
        x = self.batchnorm3(x)
        x = self.activation(x)
        z = self.dense4(x)
        encoder = keras.Model(encoder_inputs, [z], name="encoder")
        return encoder


@tf.keras.saving.register_keras_serializable()
class Cw2Decoder(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.latent_dim = args["latent_dim"]
        self.activation = layers.Activation("relu")
        self.dense1 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm1 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense2 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm2 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense3 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm3 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense4 = layers.Dense(28 * 28, activation="sigmoid")
        self.reshape = layers.Reshape([28, 28, 1])

    def build(self):
        latent_inputs = keras.Input(shape=(self.latent_dim,))
        x = self.dense1(latent_inputs)
        x = self.batchnorm1(x)
        x = self.activation(x)
        x = self.dense2(x)
        x = self.batchnorm2(x)
        x = self.activation(x)
        x = self.dense3(x)
        x = self.batchnorm3(x)
        x = self.activation(x)
        x = self.dense4(x)
        decoder_outputs = self.reshape(x)
        decoder = keras.Model(latent_inputs, decoder_outputs, name="decoder")
        return decoder


@tf.keras.saving.register_keras_serializable()
class ConeshapedEncoder(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.activation = layers.Activation("relu")
        self.flatten = layers.Flatten()
        self.dense1 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm1 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense2 = layers.Dense(128, use_bias=args["bias"])
        self.batchnorm2 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense3 = layers.Dense(64, use_bias=args["bias"])
        self.batchnorm3 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense4 = layers.Dense(args["latent_dim"], name="z")

    def build(self):
        encoder_inputs = keras.Input(shape=(28, 28, 1))
        x = self.flatten(encoder_inputs)
        x = self.dense1(x)
        x = self.batchnorm1(x)
        x = self.activation(x)
        x = self.dense2(x)
        x = self.batchnorm2(x)
        x = self.activation(x)
        x = self.dense3(x)
        x = self.batchnorm3(x)
        x = self.activation(x)
        z = self.dense4(x)
        encoder = keras.Model(encoder_inputs, [z], name="encoder")
        return encoder


@tf.keras.saving.register_keras_serializable()
class ConeshapedDecoder(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.latent_dim = args["latent_dim"]
        self.activation = layers.Activation("relu")
        self.dense1 = layers.Dense(64, use_bias=args["bias"])
        self.batchnorm1 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense2 = layers.Dense(128, use_bias=args["bias"])
        self.batchnorm2 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense3 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm3 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense4 = layers.Dense(28 * 28, activation="sigmoid")
        self.reshape = layers.Reshape([28, 28, 1])

    def build(self):
        latent_inputs = keras.Input(shape=(self.latent_dim,))
        x = self.dense1(latent_inputs)
        x = self.batchnorm1(x)
        x = self.activation(x)
        x = self.dense2(x)
        x = self.batchnorm2(x)
        x = self.activation(x)
        x = self.dense3(x)
        x = self.batchnorm3(x)
        x = self.activation(x)
        x = self.dense4(x)
        decoder_outputs = self.reshape(x)
        decoder = keras.Model(latent_inputs, decoder_outputs, name="decoder")
        return decoder


@tf.keras.saving.register_keras_serializable()
class InverseConeshapedEncoder(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.activation = layers.Activation("relu")
        self.flatten = layers.Flatten()
        self.dense1 = layers.Dense(64, use_bias=args["bias"])
        self.batchnorm1 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense2 = layers.Dense(128, use_bias=args["bias"])
        self.batchnorm2 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense3 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm3 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense4 = layers.Dense(args["latent_dim"], name="z")

    def build(self):
        encoder_inputs = keras.Input(shape=(28, 28, 1))
        x = self.flatten(encoder_inputs)
        x = self.dense1(x)
        x = self.batchnorm1(x)
        x = self.activation(x)
        x = self.dense2(x)
        x = self.batchnorm2(x)
        x = self.activation(x)
        x = self.dense3(x)
        x = self.batchnorm3(x)
        x = self.activation(x)
        z = self.dense4(x)
        encoder = keras.Model(encoder_inputs, [z], name="encoder")
        return encoder


@tf.keras.saving.register_keras_serializable()
class InverseConeshapedDecoder(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.latent_dim = args["latent_dim"]
        self.activation = layers.Activation("relu")
        self.dense1 = layers.Dense(256, use_bias=args["bias"])
        self.batchnorm1 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense2 = layers.Dense(128, use_bias=args["bias"])
        self.batchnorm2 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense3 = layers.Dense(64, use_bias=args["bias"])
        self.batchnorm3 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense4 = layers.Dense(28 * 28, activation="sigmoid")
        self.reshape = layers.Reshape([28, 28, 1])

    def build(self):
        latent_inputs = keras.Input(shape=(self.latent_dim,))
        x = self.dense1(latent_inputs)
        x = self.batchnorm1(x)
        x = self.activation(x)
        x = self.dense2(x)
        x = self.batchnorm2(x)
        x = self.activation(x)
        x = self.dense3(x)
        x = self.batchnorm3(x)
        x = self.activation(x)
        x = self.dense4(x)
        decoder_outputs = self.reshape(x)
        decoder = keras.Model(latent_inputs, decoder_outputs, name="decoder")
        return decoder


@tf.keras.saving.register_keras_serializable()
class LcwGenerator(keras.Model):
    def __init__(self, args):
        super().__init__()
        self.noise_dim = args["noise_dim"]
        self.activation = layers.Activation("relu")
        self.dense1 = layers.Dense(512)
        self.batchnorm1 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense2 = layers.Dense(512)
        self.batchnorm2 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense3 = layers.Dense(512)
        self.batchnorm3 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense4 = layers.Dense(512)
        self.batchnorm4 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense5 = layers.Dense(512)
        self.batchnorm5 = layers.BatchNormalization() if args["batch_norm"] else layers.Identity()
        self.dense6 = layers.Dense(args["latent_dim"], name="z")

    def build(self):
        noise_inputs = keras.Input(shape=(self.noise_dim,))
        x = self.dense1(noise_inputs)
        x = self.batchnorm1(x)
        x = self.activation(x)
        x = self.dense2(x)
        x = self.batchnorm2(x)
        x = self.activation(x)
        x = self.dense3(x)
        x = self.batchnorm3(x)
        x = self.activation(x)
        x = self.dense4(x)
        x = self.batchnorm4(x)
        x = self.activation(x)
        x = self.dense5(x)
        x = self.batchnorm5(x)
        x = self.activation(x)
        z = self.dense6(x)
        latent_generator = keras.Model(noise_inputs, [z], name="latent_generator")
        return latent_generator


def get_architecture(args):
    if args["architecture_type"] == "standard":
        return StandardEncoder(args).build(), StandardDecoder(args).build()
    elif args["architecture_type"] == "cw2":
        return Cw2Encoder(args).build(), Cw2Decoder(args).build()
    elif args["architecture_type"] == "coneshaped":
        return ConeshapedEncoder(args).build(), ConeshapedDecoder(args).build()
    elif args["architecture_type"] == "inverse_coneshaped":
        return InverseConeshapedEncoder(args).build(), InverseConeshapedDecoder(args).build()
